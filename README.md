# Environments for lb-conda

This repository contains the YAML files which define the software available from the `lb-conda` command.
For information about using the environments see [`lhcb-core/lbcondawrappers`](https://gitlab.cern.ch/lhcb-core/lbcondawrappers/).

## Adding an environment

To add a new environment create a new file under the `environments/` directory of this repository.
Upon being merged the environment will be automatically deployed to CVMFS by GitLab CI.

### What can be included in an environment

To be included packages should be available from conda-forge.

In the case of a Python package, the simplest first step is to make it pip-installable by add it to the Python Package Index (PyPI).
For documentation about how to do this see the [Python Packaging Guide](https://packaging.python.org/en/latest/tutorials/packaging-projects/).
If you're distributing a Pure Python package (i.e. no compilled code) and unsure what to use, we recommend using [`setuptools` with `pyproject.toml`](https://setuptools.pypa.io/en/latest/userguide/quickstart.html).
Additionally, consider adding the `lhcbsoft` as an owner of the package on PyPI so it's possible to keep ownership of the package even if people leave the collaboration.

Once the package is available with `pip install PACKAGE_NAME`, it can be added to conda-forge via [`staged-recipes`](https://github.com/conda-forge/staged-recipes#about).
Typically [`grayskull`](https://github.com/conda-forge/staged-recipes#about) can be used to automatically generate the conda-forge package recipe.

If this workflow appears to not be appropriate for your package please open an issue.

## Updating an environment

Environments are never updated to help maintain reproducibility.
Instead, each time one of the files under `environments/` is modified, the given environment is created with a new "version" (corresponding to the current date and time).
This way users of `lb-conda` can always switch back to a previous version.
The modification to the environment YAML definition can either be to the definition of the environment or to simply add a comment.

If you are regularly updating an environment, feel free to request merge permissions on this repository.
