#!/usr/bin/env python3
import argparse
import yaml
from pathlib import Path


def find_pip_index(data):
    for i, dep in enumerate(data.get("dependencies", [])):
        if isinstance(dep, dict) and "pip" in dep:
            print("Found pip dependencies in yaml")
            return i


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("reference_yaml", type=Path)
    parser.add_argument("rendered_yaml", type=Path)
    args = parser.parse_args()

    reference = yaml.safe_load(args.reference_yaml.read_text())
    rendered = yaml.safe_load(args.rendered_yaml.read_text())

    if idx := find_pip_index(rendered):
        rendered["dependencies"][idx] = reference["dependencies"][find_pip_index(reference)]

    args.rendered_yaml.write_text(yaml.safe_dump(rendered))



if __name__ == "__main__":
    parse_args()
