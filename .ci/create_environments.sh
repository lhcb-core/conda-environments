#!/usr/bin/env bash
yum install -y bzip2
MAMBA_ROOT_PREFIX=$(mktemp -d)
export MAMBA_ROOT_PREFIX
curl -L --silent "https://micromamba.snakepit.net/api/micromamba/linux-64/latest" | tar -xvj bin/micromamba
eval "$(./bin/micromamba shell hook -s bash --root-prefix "$PWD/micromamba")"
micromamba create --yes --prefix /tmp/prefix -c conda-forge python git gitpython mamba pyyaml conda-lock go-yq pip
micromamba activate /tmp/prefix
pip install git+https://github.com/conda/conda-lock@2da5924571e292e70f1c03f1ca215962f34de281

set -euo pipefail
IFS=$'\n\t'

rm -rf environments-to-deploy/
mkdir -p environments-to-deploy/

for env_name in $(.ci/find_new_environments.py); do
    timestamp=$(date -u '+%Y-%m-%d_%H-%M')

    echo "Creating environment for ${env_name}/${timestamp}"
    mkdir -p "environments-to-deploy/${env_name}/${timestamp}"

    # Ensure the YAML is valid
    yq "environments/${env_name}.yaml" > /dev/null

    if ! yq -e '.channels[]' "environments/${env_name}.yaml" | grep -E '^nodefaults$' &> /dev/null; then
        echo "ERROR: nodefaults is not included in the channel list"
        exit 1
    fi

    for platform in $(yq -e '(.platforms[] // ["linux-64"][])' "environments/${env_name}.yaml"); do
        conda-lock -f "environments/${env_name}.yaml" --lockfile "environments-to-deploy/${env_name}/${timestamp}/${platform}.yaml" --platform "${platform}" --micromamba
    done
done
