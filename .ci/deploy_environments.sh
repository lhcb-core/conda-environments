#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'


function do_deploy() {
    yaml_path="$1"
    name=$(dirname "${yaml_path}")
    subdir=$(basename "${yaml_path}")
    subdir=${subdir%.*}

    echo "Deploying ${name} for ${subdir} from ${yaml_path}"
    task_uuid=$(curl -L -X PUT \
        -H "Authorization: Bearer ${REQUEST_DEPLOY_INSTALL_TOKEN}" \
        --data-binary "@${yaml_path}" \
        "https://lhcb-core-tasks.web.cern.ch/hooks/conda-deploy/deploy/v2/${name}/?conda_subdir=${subdir}" | cut -d '"' -f 2)
    echo "Sucessfully sent task with ID ${task_uuid}"

    while true; do
        current_status=$(curl --silent -L "https://lhcb-core-tasks.web.cern.ch/tasks/status/$task_uuid" | cut -d '"' -f 2);
        echo "$(date -u): Status is ${current_status}";
        if [[ "${current_status}" == "SUCCESS" ]]; then
            exit 0;
        elif [[ "${current_status}" == "FAILURE" ]]; then
            exit 1;
        else
            sleep 30;
        fi
    done
}
export -f do_deploy

cd "environments-to-deploy/"
# Can't use find as it exits with 0 even if "do_deploy" fails
# https://apple.stackexchange.com/a/49156
find * -name '*.yaml' -type f -print0 | xargs -0 -I @ bash -c "do_deploy '@'"
